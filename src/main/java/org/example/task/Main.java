package org.example.task;

import org.example.task.entity.Card;
import org.example.task.entity.User;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        User[] userEntities = new User[100];
        int count = 0;
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("=========================");
            System.out.println("Special coffe machine ");
            System.out.println("=========================");
            System.out.println("1-registration");
            System.out.println("2-Login");
            int n = scanner.nextInt();
            switch (n) {
                case 1: {
                    System.out.print("First name:");
                    String firstName = scanner.next();
                    System.out.print("Last name:");
                    String lastName = scanner.next();
                    System.out.print("Email:");
                    String email = scanner.next();
                    System.out.print("Phone number:");
                    String phoneNumber = scanner.next();
                    System.out.print("Password:");
                    String password = scanner.next();
                    User u1 = new User(firstName, lastName, email, phoneNumber, password);
                    userEntities[count] = u1;
                    count++;
                    System.out.println("succesfully registered!");
                    break;
                }
                case 2: {
                    System.out.print("Phone number:");
                    String phoneNumber = scanner.next();
                    System.out.print("Password:");
                    String password = scanner.next();
                    int index = -1;
                    for (int i = 0; i < count; i++) {
                        if (userEntities[i].getPhoneNumber().equals(phoneNumber) && userEntities[i].getPassword().equals(phoneNumber)) {
                            index = i;
                            break;
                        }
                    }
                    if (index == -1) {
                        System.out.println("phone number or password incorrect");
                    } else {
                        System.out.println(userEntities[index].getFirstName() + " " + userEntities[index].getLastName() + " welcome to coffe machine!!!");
                        System.out.println("=======================================================================");
                        System.out.println("If you want you can registr your card to buy coffe from any location!!!");
                        System.out.println("=======================================================================");
                        System.out.println("1-> just buy coffe");
                        System.out.println("2-> registr credit card to buy coffe anywhere easly");
                        int cardRegisterOrCoffee = scanner.nextInt();
                        switch (cardRegisterOrCoffee) {
                            case 1: {
                                System.out.println("1->Coppuchino");
                                System.out.println("2->Black coffee");
                                System.out.println("3->Tea");
                                System.out.println("4->Back");
                                int coffeType = scanner.nextInt();
                                    switch (coffeType) {
                                        case 1: {
                                            System.out.println("Your coppucino is ready !!!");
                                            System.out.println("Thanks for choosing our service!!!");
                                            break;
                                        }
                                        case 2: {
                                            System.out.println("Your black coffee is ready !!!");
                                            System.out.println("Thanks for choosing our service!!!");
                                            break;
                                        }
                                        case 3: {
                                            System.out.println("Your tea is ready !!!");
                                            System.out.println("Thanks for choosing our service!!!");
                                           break;
                                        }
                                        case 4: {
                                           break;
                                        }
                                    }
                                    break;
                            }
                            case 2: {
                                System.out.println("=======================");
                                System.out.println("Write your card details");
                                System.out.println("=======================");
                                System.out.print("Card name:");
                                String cardName = scanner.next();
                                System.out.print("Card number:");
                                String cardNumber = scanner.next();
                                System.out.print("Expire date:");
                                String expirationDate = scanner.next();
                                System.out.print("Security code:");
                                String securityCode = scanner.next();
                                Card card = new Card(userEntities[index], cardName, cardNumber, expirationDate, securityCode);
                                System.out.println(card + "succesfully registered");
                                System.out.println("Now you can easly buy anywhere coffe from our coffee machines with your login and password !!!");
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
