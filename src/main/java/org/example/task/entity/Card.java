package org.example.task.entity;

public class Card {

    private User user;
    private String cardName;
    private String cardNumber;
    private String expirationDate;
    private String securityCode;

    public Card(User user, String cardName, String cardNumber, String expirationDate, String securityCode) {
        this.user = user;
        this.cardName = cardName;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.securityCode = securityCode;
    }

    public User getUserEntity() {
        return user;
    }

    public void setUserEntity(User user) {
        this.user = user;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    @Override
    public String toString() {
        return "CardEntity{" +
                "userEntity=" + user +
                ", cardName='" + cardName + '\'' +
                ", cardNumber=" + cardNumber +
                ", expirationDate=" + expirationDate +
                ", securityCode=" + securityCode +
                '}';
    }
}
